//
//  WebViewController.swift
//  WKWebView
//
//  Created by Peng on 2019/05/20.
//  Copyright © 2019年
//

import UIKit
import WebKit
import SwiftSoup
class WebViewController: UIViewController {
    var mWebView: WKWebView? = nil
    var mWebViewBack: WKWebView? = nil
    @IBOutlet weak var mBackBtn: UIButton!
    @IBOutlet weak var mForwardBtn: UIButton!
    @IBOutlet weak var mCheckBtn: UIButton!
    var imagelink1: String = ""
    var imagelink2: String = ""
    var imagelink3: String = ""
    var itemURL: String = ""
    var itemASIN: String = ""
    var itemTITLE: String = ""
    var itemPRICE: String = ""
    var itemTYPE: String = ""
    var itemREMAIN: String = ""
    var itemSELECTNUM: String = ""
    
    typealias Item = (text: String, html: String)
    // current document
    var document: Document = Document.init("")
    // item founds
    var items: [Item] = []
    var doc: String = ""
    let outArray = [
        "A4424478051",
        "A3571471051",
        "A4210960051",
        "A2277724051",
        "A393994011",
        "A346154011",
        "A345991011",
        "A352484011",
        "A2221071051",
        "A325315011",
        "A3485873051",
        "A2799399051",
        "A465392",
        "A561956"]
    var ngtime = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        itemURL = "https://www.amazon.co.jp/dp/B07QVQVZWW"
        //var tmpURL = itemURL
        //tmpURL.append("\"")
        //itemASIN = getsubstr(substring1: "dp",substring2: "\"",oristr: tmpURL)
        let image = UIImage(named: "blueicon") // blueiconという名前の画像
        mCheckBtn.setBackgroundImage(image, for: .normal) // 背景に画像をset
        mCheckBtn.isEnabled = false;
        //mCheckBtn.setImage(UIImage(named: "blueicon")?.withRenderingMode(.alwaysOriginal), for: .normal)
        loadURL(urlString: itemURL)
        
    }
    
    // View構築後の処理
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // アラート表示
        //downloadHTML()
        //self.showAlert("Alert　Message!!")
    }
    // アラート表示の処理
    func showAlert(_ alertMessage: String) {
        let alertConroller = UIAlertController(
            title: "Alert", message: alertMessage, preferredStyle: .alert)
        let originAction = UIAlertAction(
            title: "OK", style: .default, handler: nil)
        alertConroller.addAction(originAction)
        self.present(alertConroller, animated: true, completion: nil)
    }
    
    func downloadHTML(urlString: String) {
        // url string to URL
        guard let url = URL(string: urlString) else {
            // an error occurred
            self.showAlert("Error: doesn't seem to be a valid URL")
            
            return
        }
        
        do {
            
            let html = try String.init(contentsOf: url)
            // parse it into a Document
            document = try SwiftSoup.parse(html)
            let elements = try document.getAllElements()
            itemTITLE = try document.title()
            let tmpprice: String = try elements.outerHtml();
            if (tmpprice.contains("font color=\"red\"")) {
                itemPRICE = getsubstr(substring1: "font color=\"red\"",substring2: "<",oristr: tmpprice)
                print(itemPRICE)
                /*let tmp = itemPRICE.replacingOccurrences(of: "¥", with: "").replacingOccurrences(of: ",", with: "")
                print(tmp)
                let a:Int? = Int(tmp)
                print(a!*10)
                let format = NumberFormatter()
                format.numberStyle = .decimal
                let string = format.string(from: NSNumber(value: a!*10))
                print(string!)*/
            }
            else {
                itemPRICE = "-1"
                print(itemPRICE)
            }
            //print(tmpprice)
            
            let tmp = try elements.select("a")
            //let tmp2 = try tmp.select("imgsrc")
            for element: Element in tmp.array() {
                let linkHref: String = try element.outerHtml();
                //print(element)
                if linkHref.contains("img src") {
                    imagelink1 = getsubstr(substring1: "img src=",substring2: "\"",oristr: linkHref)
                    /*imagelink1 = getsubstr(substring1: "img src=",substring2: "\"",oristr: linkHref).replacingOccurrences(of: "110", with: "360")
                    imagelink2 = getsubstr(substring1: "img src=",substring2: "\"",oristr: linkHref).replacingOccurrences(of: "110", with: "360")
                    imagelink3 = getsubstr(substring1: "img src=",substring2: "\"",oristr: linkHref).replacingOccurrences(of: "110", with: "360")*/
                }
            }
            
            //print(value)
            //parse()
        } catch let error {
            // an error occurred
            self.showAlert("Error: \(error)")
            
        }
        
    }
    func getsubstr(substring1: String, substring2: String, oristr: String) -> String {
        var index = 0
        var startidx = 0
        var endidx = 0
        var substr = ""
        // Loop through parent string looing for the first character of the substring
        for char in oristr.characters {
            if startidx == 0 {
                if substring1.characters.first == char {
                    let startOfFoundCharacter = oristr.index(oristr.startIndex, offsetBy: index)
                    let lengthOfFoundCharacter = oristr.index(oristr.startIndex, offsetBy: (substring1.characters.count + index))
                    let range = startOfFoundCharacter..<lengthOfFoundCharacter
                    if oristr.substring(with: range) == substring1 {
                        startidx = substring1.characters.count + index + 1
                    }
                }
            }
            else {
                if substring2.characters.first == char {
                    let startOfFoundCharacter = oristr.index(oristr.startIndex, offsetBy: index)
                    let lengthOfFoundCharacter = oristr.index(oristr.startIndex, offsetBy: (substring2.characters.count + index))
                    let range = startOfFoundCharacter..<lengthOfFoundCharacter
                    if oristr.substring(with: range) == substring2 {
                        if (startidx-1 != index)
                        {
                            endidx = index
                            let str = oristr.index(oristr.startIndex, offsetBy: startidx)
                            let end = oristr.index(oristr.startIndex, offsetBy: endidx)
                            let finalrange = str..<end
                            substr = oristr.substring(with: finalrange)
                            break
                        }
                    }
                }
            }
            index += 1
        }
        return substr
    }
    
    //Parse CSS selector
    func parse() {
        var temp = ""
        do {
            //empty old items
            items = []
            // firn css selector
            let elements: Elements = try document.select("a[href]")
            //transform it into a local object (Item)
            //var startidx:String.Index = 0
            //var endidx = 0
            for element in elements {
                let text = try element.text()
                let linkHref: String = try element.attr("href")
                let html = try element.html()
                items.append(Item(text: text, html: html))
                temp.append(linkHref)
                //temp.append("/n")
                
                if linkHref.contains("B00HRRXUUQ") {
                    ngtime += 1
                    print("●ブラックリスト商品です。")
                }
                
                if linkHref.contains("node=") {
                    for outitem in outArray {
                        //print (outitem)
                        if (linkHref.contains(outitem))
                        {
                            ngtime += 1
                            print("●対象外商品です。")
                        }
                    }
                }
            }
            
        } catch let error {
            self.ngtime += 1
            let alertController = UIAlertController(title: "Error", message: "\(error)", preferredStyle: .alert)
            self.present(alertController, animated: true, completion: nil)
        }
        
        //tableView.reloadData()
    }
    
    private func loadURL(urlString: String) {
        let url = URL(string: urlString)
        if let url = url {
            mBackBtn.isEnabled = false
            mForwardBtn.isEnabled = false
            let request = URLRequest(url: url)
            // init and load request in webview.
            mWebView = WKWebView(frame: self.view.frame)
            mWebView?.frame.origin.y = mBackBtn.frame.origin.y + CGFloat(40)
            //mWebView?.customUserAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.109 Safari/537.36"
            //load URL here
            mWebViewBack = WKWebView(frame: self.view.frame)
            mWebViewBack?.frame.origin.y = mBackBtn.frame.origin.y + CGFloat(40)
            mWebViewBack?.customUserAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.109 Safari/537.36"
            if let mWebView = mWebView {
                mWebView.navigationDelegate = self
                mWebView.addObserver(self, forKeyPath: "URL", options: .new, context: nil)
                mWebView.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: nil)
                mWebView.load(request)
                self.view.addSubview(mWebView)
                self.view.sendSubviewToBack(mWebView)
            }
            if let mWebViewBack = mWebViewBack {
                mWebViewBack.navigationDelegate = self
                mWebViewBack.load(request)
                //self.view.addSubview(mWebViewBack)
                //self.view.sendSubviewToBack(mWebViewBack)
            }
            //var webView = WKWebView()
            
            
        }
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == #keyPath(WKWebView.url) {
            //print("### URL:", self.mWebView?.url!)
            print(self.mWebView?.url?.absoluteString)
            //Disable to login and register
            if (self.mWebView?.url?.absoluteString.contains("signin"))! {
                let alertController = UIAlertController(title: "Error", message: "You can not register and login", preferredStyle: .alert)
                let defaultAction: UIAlertAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler:{
                    (action: UIAlertAction!) -> Void in
                    print("OK")
                    self.mWebView?.goBack()
                })
                alertController.addAction(defaultAction)
                self.present(alertController, animated: true, completion: nil)
            }
            if ((self.mWebView?.url?.absoluteString.contains("www.amazon.co.jp/gp/aw/d/"))! || (self.mWebView?.url?.absoluteString.contains("/dp/"))!)
            {
                var newURL = "https://www.amazon.co.jp/dp/"
                if ((self.mWebView?.url?.absoluteString.contains("www.amazon.co.jp/gp/aw/d/"))!){
                    //print(self.getsubstr(substring1: "aw/d/",substring2: "/",oristr: (self.mWebView?.url!.absoluteString)!))
                    //print("hotest1")
                    let tempURL: String = "\(self.mWebView?.url)"
                    newURL.append(self.getsubstr(substring1: "aw/d",substring2: ")",oristr: tempURL))
                }
                else {
                    let tempURL: String = "\(self.mWebView?.url)"
                    newURL.append(self.getsubstr(substring1: "/dp",substring2: ")",oristr: tempURL))
                    //print(self.getsubstr(substring1: "/dp",substring2: ")",oristr: tempURL))
                    //print("hotest2")
                }
                
                //print(self.mWebView?.url?.absoluteString)
                //downloadHTML(urlString: (self.mWebView?.url!.absoluteString)!)
                //let request = URLRequest(url: (self.mWebView?.url)!)
                itemREMAIN = ""
                downloadHTML(urlString: newURL)
                let request = URLRequest(url: URL(string: newURL)!)
                mWebViewBack!.load(request)
                mCheckBtn.isHidden = false
            }
            else {
                mCheckBtn.isHidden = true
            }
            mCheckBtn.setTitle("購入不可", for: .normal)
            mCheckBtn.isEnabled = false;
        }
        
        if keyPath == #keyPath(WKWebView.estimatedProgress) {
            // When page load finishes. Should work on each page reload.
            if (self.mWebView!.estimatedProgress == 1) {
                //print("### EP:", self.mWebView?.estimatedProgress)
            }
        }
    }
    
    @IBAction func checkAction(sender: UIButton) {
        var image1: UIImage = UIImage(named: "noimage")!
        do {
            if imagelink1 != ""{
                let data = try Data(contentsOf: URL(string: imagelink1)!)
                image1 = UIImage(data: data)!
            }
        }catch let err {
            print("Error : \(err.localizedDescription)")
        }
        var image2: UIImage = UIImage(named: "noimage")!
        do {
            if imagelink2 != ""{
                let data = try Data(contentsOf: URL(string: imagelink2)!)
                image2 = UIImage(data: data)!
            }
        }catch let err {
            print("Error : \(err.localizedDescription)")
        }
        var image3: UIImage = UIImage(named: "noimage")!
        do {
            if imagelink3 != ""{
                let data = try Data(contentsOf: URL(string: imagelink3)!)
                image3 = UIImage(data: data)!
            }
        }catch let err {
            print("Error : \(err.localizedDescription)")
        }
        var asinlabel = "ASIN: "
        asinlabel.append(itemASIN)
        var titlelabel = "商品名: "
        titlelabel.append(itemTITLE)
        var pricelabel = "価格: "
        pricelabel.append(itemPRICE)
        var itemtypelabel = "商品タイプ: "
        itemtypelabel.append(itemTYPE)
        /*let alert = CustomAlert(asin: asinlabel, title: titlelabel, price: pricelabel, itemtype: itemtypelabel, image1: image1, image2: image2 ,image3: image3)
        alert.show(animated: true)*/
        
        
        let oriprice = itemPRICE.replacingOccurrences(of: "¥", with: "").replacingOccurrences(of: ",", with: "")
        let oriprice95:Int = Int(Double(oriprice)! * Double(0.95))
        let oriprice97:Int = Int(Double(oriprice)! * Double(0.97))
        
        let format = NumberFormatter()
        format.numberStyle = .decimal
        let price95label = format.string(from: NSNumber(value: oriprice95))
        let price97label = format.string(from: NSNumber(value: oriprice95))
        var showmessage = "\(titlelabel)\n\n\(itemtypelabel)\n商品画像URL: \(imagelink1)\n\n\(pricelabel)\n事前入金: ¥\(price95label!)\nカード: ¥\(price97label!)\n在庫状況: \(itemREMAIN)\nセレクタに選択できる数量: \(itemSELECTNUM)\n"
        
        let alert = UIAlertController(title: asinlabel, message: showmessage, preferredStyle: .alert)
        
        var actiontitle = "OK"
        if itemSELECTNUM != ""{
            actiontitle = "カートに入れる"
        }
        let action = UIAlertAction(title: actiontitle, style: .default, handler: nil)
        
        let cartbtn = UIButton()
        cartbtn.setBackgroundImage(UIImage(named: "cart"), for: .normal)
        alert.view.addSubview(cartbtn)
        //print(alert.view.bounds.size.width)
        //print(alert.view.bounds.size.width)
        /*
        let pickerView = UIPickerView(frame:
            CGRect(x: 10, y: 300, width: 260, height: 200))
        pickerView.dataSource = self as? UIPickerViewDataSource
        pickerView.delegate = self as? UIPickerViewDelegate
        
        // comment this line to use white color
        pickerView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        
        alert.view.addSubview(pickerView)
        */
        alert.addAction(action)
        
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func backAction(_ sender: UIButton) {
        if mWebView?.goBack() == nil {
            print("No more page to back")
        }

    }
    
    @IBAction func forwardAction(_ sender: UIButton) {
        if mWebView?.goForward() == nil {
            print("No more page to forward")
        }
    }
}


extension WebViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
    }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Start to load")
        //print(webView.url?.absoluteString)
        mCheckBtn.setTitle("解析中", for: .normal)
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("finish to load")
        if let webView = mWebView {
            mForwardBtn.isEnabled = webView.canGoForward
            mBackBtn.isEnabled = webView.canGoBack
        }
        if webView.self == mWebViewBack {
            webView.evaluateJavaScript(
                "document.getElementsByTagName('html')[0].innerHTML",
                completionHandler: { (text: Any?, error: Error?) in
                    if let text = text as? String {
                        //print(text)
                        if !text.contains("<bodybgcolor=\"#FFFFFF\"") {
                            self.doc.append("●商品が存在しています。\n")
                            print("●商品が存在しています。")
                        }
                        do {
                            //let doc: Document = try SwiftSoup.parse(text)
                            self.document = try SwiftSoup.parse(text)
                            
                            let itemnumarray = try self.document.getAllElements().select("span")
                            for element: Element in itemnumarray.array() {
                                if try element.outerHtml().contains("quantity"){
                                    let itemnumarray2 = try element.select("option")
                                    self.itemSELECTNUM = String(itemnumarray2.array().count)
                                }
                            }
                            
                            //self.itemSELECTNUM = String(itemnumarray.array().count)
                            print(self.itemSELECTNUM)
                            /*for element: Element in itemnumarray.array() {
                                let elementstr = try element.outerHtml()
                                print(elementstr)
                                if (elementstr.contains("a-dropdown-container")) && (elementstr.contains("数量"))  {
                                    //print("-----------------s")
                                    //print(elementstr)
                                    //print("-----------------d")
                                    
                                    /*self.itemSELECTNUM = self.getsubstr(substring1: "option value=",substring2: "\"",oristr: elementstr)
                                    print (self.itemSELECTNUM)*/
                                }
                                
                            }*/
                            
                            let fontarray = try self.document.getAllElements().select("div")
                            for element: Element in fontarray.array() {
                                if (element.id() == "availability"){
                                    let fontarray2 = try element.select("span")
                                    for element2: Element in fontarray2.array() {
                                        if try element2.className() != "availabilityHelpLink"{
                                            print(try element2.text())
                                            self.itemREMAIN = try element2.text()
                                        }
                                    }
                                }
                            }
                            
                            //print(self.document)
                            //print(try self.document.title())
                            self.itemTITLE = try self.document.title()
                            
                            if text.contains("あわせ買い対象") {
                                self.itemTYPE = "2"
                            }
                            let tmptype = try self.document.select("input")
                            for element: Element in tmptype.array() {
                                if try element.outerHtml().contains("id=\"ASIN\"")
                                {
                                    //print(element)
                                    self.itemASIN = self.getsubstr(substring1: "value=",substring2: "\"",oristr: try element.outerHtml())
                                    //print(self.itemASIN)
                                }
                                if try element.outerHtml().contains("カートに入れる") {
                                    self.itemTYPE = "1"
                                }
                                if try element.outerHtml().contains("予約注文する") {
                                    self.itemTYPE = "3"
                                }
                            }
                            print(self.itemTYPE)
                            self.parse()
                        } catch Exception.Error(let type, let message) {
                            self.ngtime += 1
                            print(message)
                        } catch {
                            self.ngtime += 1
                            print("error")
                        }
                        if text.contains("Amazonプライム会員</a>は無料") ||
                            text.contains("Amazon.co.jp が発送") ||
                            text.contains("Amazon.co.jp</a> が販売、発送します。") ||
                            text.contains("フラストレーション・フリー・パッケージ</a>で販売、発送します。") {
                            //self.doc.append("●商品が存在しています。\n")
                            print("●商品が購入可能です。")
                        }
                        else {
                            print("●商品購入できません。")
                            self.ngtime += 1
                        }
                    }
                    if self.ngtime == 0{
                        //print(self.getsubstr(substring1: "\"hiRes\":",substring2: "\"",oristr: tmptext))
                        //self.imagelink2 = self.getsubstr(substring1: "\"hiRes\":",substring2: "\"",oristr: tmptext)
                        //self.imagelink3 = self.getsubstr(substring1: "\"hiRes\":",substring2: "\"",oristr: tmptext)
                        self.mCheckBtn.setTitle("購入可能", for: .normal)
                        self.mCheckBtn.isEnabled = true;
                    }
                    else {
                        self.mCheckBtn.setTitle("購入不可", for: .normal)
                        self.mCheckBtn.isEnabled = false;
                    }
                    } as? (Any?, Error?) -> Void
            )
        }
        /*if webView.self == mWebView {
            webView.evaluateJavaScript(
                "document.getElementsByTagName('html')[0].innerHTML",
                completionHandler: { (text: Any?, error: Error?) in
                    if let text = text as? String {
                        var tmptext1 = ""
                        var tmptext2 = ""
                        var tmptext3 = ""
                        //print(text)
                        //let doc = try SwiftSoup.parse(text)
                        //print(try doc.select("a[href]"))
                        self.imagelink1 = self.getsubstr(substring1: "hires=",substring2: "\"",oristr: text)
                        if self.imagelink1 != "" {
                            var replaceold = "hires=\""
                            replaceold.append(self.imagelink1)
                            replaceold.append("\"")
                            //print(replaceold)
                            tmptext1 = text.replacingOccurrences(of: replaceold, with: "pengtest")
                            //print(tmptext)
                        }
                        self.imagelink2 = self.getsubstr(substring1: "hires=",substring2: "\"",oristr: tmptext1)
                        if self.imagelink2 != "" {
                            var replaceold = "hires=\""
                            replaceold.append(self.imagelink2)
                            replaceold.append("\"")
                            //print(replaceold)
                            tmptext2 = tmptext1.replacingOccurrences(of: replaceold, with: "pengtest")
                            //print(tmptext)
                        }
                        self.imagelink3 = self.getsubstr(substring1: "hires=",substring2: "\"",oristr: tmptext2)
                        /*if self.imagelink3 != "" {
                            var replaceold = "hires=\""
                            replaceold.append(self.imagelink3)
                            replaceold.append("\"")
                            //print(replaceold)
                            tmptext2 = text.replacingOccurrences(of: replaceold, with: "pengtest")
                            //print(tmptext)
                        }*/
                    }
                } as? (Any?, Error?) -> Void
            )
        }*/
        
        /*let alertController = UIAlertController(title: "result", message: self.doc, preferredStyle: .alert)
        self.present(alertController, animated: true, completion: nil)*/
        //print(temp)
    }
}
extension UIDevice {
    var hasNotch: Bool {
        let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        return bottom > 0
    }
}

