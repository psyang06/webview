//
//  CustomAlert.swift
//  WKWebview
//
//  Created by Peng on 2019/05/20.
//  Copyright © 2019年
//

import UIKit

class CustomAlert: UIView, Modal {
    var backgroundView = UIView()
    var dialogView = UIView()
    var scrollView = UIScrollView()
    
    
    convenience init(asin:String,title:String,price:String,itemtype:String,image1:UIImage,image2:UIImage,image3:UIImage) {
        self.init(frame: UIScreen.main.bounds)
        initialize(asin: asin, title: title, price: price, itemtype: itemtype, image1: image1, image2: image2, image3: image3)
        
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func initialize(asin:String, title:String, price:String, itemtype:String, image1:UIImage, image2:UIImage, image3:UIImage){
        dialogView.clipsToBounds = true
        
        backgroundView.frame = frame
        backgroundView.backgroundColor = UIColor.black
        backgroundView.alpha = 0.6
        backgroundView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTappedOnBackgroundView)))
        addSubview(backgroundView)
        
        let dialogViewWidth = frame.width-32
        
        let asinLabel = UILabel(frame: CGRect(x: 8, y: 8, width: dialogViewWidth-16, height: 30))
        asinLabel.text = asin
        asinLabel.textAlignment = .center
        dialogView.addSubview(asinLabel)
        
        let titleLabel = UILabel(frame: CGRect(x: 8, y: asinLabel.frame.height + asinLabel.frame.origin.y + 1, width: dialogViewWidth-16, height: 30))
        titleLabel.text = title
        titleLabel.textAlignment = .center
        dialogView.addSubview(titleLabel)
        
        let itemtypeLabel = UILabel(frame: CGRect(x: 8, y: titleLabel.frame.height + titleLabel.frame.origin.y + 1, width: dialogViewWidth-16, height: 30))
        itemtypeLabel.text = itemtype
        itemtypeLabel.textAlignment = .center
        dialogView.addSubview(itemtypeLabel)
        
        let separatorLineView = UIView()
        separatorLineView.frame.origin = CGPoint(x: 0, y: itemtypeLabel.frame.height + itemtypeLabel.frame.origin.y + 1)
        separatorLineView.frame.size = CGSize(width: dialogViewWidth, height: 1)
        separatorLineView.backgroundColor = UIColor.groupTableViewBackground
        dialogView.addSubview(separatorLineView)
        
        
        let imageView = UIImageView()
        imageView.frame.origin = CGPoint(x: 8, y: separatorLineView.frame.height + separatorLineView.frame.origin.y + 8)
        imageView.frame.size = CGSize(width: dialogViewWidth - 16 , height: dialogViewWidth - 16)
        imageView.image = image1
        imageView.layer.cornerRadius = 4
        imageView.clipsToBounds = true
        dialogView.addSubview(imageView)
        
        let imageView2 = UIImageView()
        imageView2.frame.origin = CGPoint(x: 8, y: imageView.frame.height + imageView.frame.origin.y + 8)
        imageView2.frame.size = CGSize(width: dialogViewWidth - 16 , height: dialogViewWidth - 16)
        imageView2.image = image2
        imageView2.layer.cornerRadius = 4
        imageView2.clipsToBounds = true
        dialogView.addSubview(imageView2)
        
        let imageView3 = UIImageView()
        imageView3.frame.origin = CGPoint(x: 8, y: imageView2.frame.height + imageView2.frame.origin.y + 8)
        imageView3.frame.size = CGSize(width: dialogViewWidth - 16 , height: dialogViewWidth - 16)
        imageView3.image = image3
        imageView3.layer.cornerRadius = 4
        imageView3.clipsToBounds = true
        dialogView.addSubview(imageView3)
        
        let cartbtn = UIButton()
        cartbtn.frame.origin = CGPoint(x: 8, y: imageView3.frame.height + imageView3.frame.origin.y + 8)
        cartbtn.frame.size = CGSize(width: dialogViewWidth - 16 , height: 45)
        cartbtn.setBackgroundImage(UIImage(named: "cart"), for: .normal)
        //cartbtn.image = image3
        cartbtn.layer.cornerRadius = 4
        cartbtn.clipsToBounds = true
        dialogView.addSubview(cartbtn)
        
        let priceLabel = UILabel(frame: CGRect(x: 8, y: cartbtn.frame.height + cartbtn.frame.origin.y + 8, width: dialogViewWidth-16, height: 30))
        priceLabel.text = price
        priceLabel.textAlignment = .center
        dialogView.addSubview(priceLabel)
        
        let dialogViewHeight = asinLabel.frame.height + 1 + titleLabel.frame.height + 1 + itemtypeLabel .frame.height + 1 +  separatorLineView.frame.height + 1 + imageView.frame.height + 8 + imageView.frame.height + 8 + imageView.frame.height + 8 + cartbtn.frame.height + 8 + priceLabel.frame.height + 8
        
        dialogView.frame.origin = CGPoint(x: 32, y: frame.height)
        dialogView.frame.size = CGSize(width: frame.width-32, height: dialogViewHeight)
        dialogView.backgroundColor = UIColor.white
        dialogView.layer.cornerRadius = 6
        //addSubview(dialogView)
        
        let rect  = CGRect(x: 16, y: 40, width: frame.width-32, height: frame.height-80)
        scrollView = UIScrollView(frame: rect)
        scrollView.backgroundColor = UIColor.white
        
        scrollView.contentSize = CGSize(width: frame.width-32, height: dialogViewHeight)
        scrollView.bounces = false
       /* if #available(iOS 11.0, *) {
            let offset = CGPoint(x: -scrollView.adjustedContentInset.left, y: -scrollView.adjustedContentInset.top)
            scrollView.setContentOffset(offset, animated: false)
        } else {
            let offset = CGPoint(x: -scrollView.contentInset.left, y: -scrollView.contentInset.top)
            scrollView.setContentOffset(offset, animated: false)
        }
        scrollView.setContentOffset(CGPoint(x: 16,y: -scrollView.contentInset.top-300), animated: false)
        scrollView.autoresizingMask = [.flexibleWidth, .flexibleHeight]*/

        //scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
        
        // status bar 余白をとる
        let edgeInsets = UIEdgeInsets(top: dialogViewHeight/4, left: -16, bottom: 0, right: 0)
        // 上の余白部分
        scrollView.contentInset = edgeInsets
        //  スクロールバーの開始位置をずらす
        scrollView.scrollIndicatorInsets = edgeInsets
        
        scrollView.addSubview(dialogView)
        addSubview(scrollView)
        
        
        /*scrollView.frame.origin = CGPoint(x: 32, y: frame.height)
        scrollView.frame.size = CGSize(width: frame.width-32, height: dialogViewHeight)
        scrollView.backgroundColor = UIColor.white
        scrollView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        scrollView.layer.cornerRadius = 6
        
        scrollView.addSubview(dialogView)
        addSubview(scrollView)*/
    }
    
    @objc func didTappedOnBackgroundView(){
        dismiss(animated: true)
    }
    
}
